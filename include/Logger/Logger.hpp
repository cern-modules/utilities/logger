#pragma once

/*
 * Author: Jorge Camarero Vera
 * @Copyright
 */

#include <spdlog/fmt/ostr.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/spdlog.h>

#include <memory>
#include <string>

#define LOGGER_FILENAME "my_cern_project.log"
#define LOGGER_ENABLE_LOGGING \
  "LOGGER_ENABLE_LOGGING"  // this env variable has to be set in order to enable logger at all
#define LOGGER_PRINT_STDOUT \
  "LOGGER_PRINT_STDOUT"  // set this env variable to 1 to enable prints on stdout
#define LOGGER_ENABLE_DEBUG \
  "LOGGER_ENABLE_DEBUG"  // set this env variable to 1 to enable debug logs

namespace utility {

struct LoggerParameterInitializer {
  LoggerParameterInitializer();
};

class Logger {
 public:
  using loggerPtr = std::shared_ptr<spdlog::logger>;
  Logger() = delete;
  explicit Logger(const std::string& name);

  ~Logger() = default;
  Logger(const Logger& other) = default;
  Logger(Logger&& r) = delete;
  Logger& operator=(const Logger& other) = delete;
  Logger& operator=(Logger&& other) = delete;

  loggerPtr operator->() const { return logger_; }

 private:
  static const std::shared_ptr<spdlog::sinks::rotating_file_sink_mt> file_sink_;
  static const LoggerParameterInitializer initializer_;
  loggerPtr logger_;
};

}  // namespace utility
