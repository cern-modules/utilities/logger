[![pipeline status](https://gitlab.com/docker-jcv/my_docker_images/badges/master/pipeline.svg)](https://gitlab.com/docker-jcv/my_docker_images/-/commits/master)

# Logger

It allows logs with levels in the code. It's based on [spdlog](https://github.com/gabime/spdlog).

## How to use

### Stdout Mode

```Bash
LOGGER_ENABLE_LOGGING=1 LOGGER_PRINT_STDOUT=1 LOGGER_ENABLE_DEBUG=1 ./Application
```

### File Mode

```Bash
LOGGER_ENABLE_LOGGING=1 LOGGER_ENABLE_DEBUG=1 ./Application
```
## Clang-format

```bash
./scripts/format.sh
```
