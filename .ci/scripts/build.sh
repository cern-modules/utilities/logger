#!/bin/bash

set -ex
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ../
make -j$((`nproc`-2))
