#!/bin/bash

TIMEOUT=180

cd build
LOG_DIR=utLogs
mkdir ${LOG_DIR}
retCode=0

for ut in `ls ./bin/ | grep ^ut-`
do
    rm my_cern_project.log
    LOGGER_ENABLE_LOGGING=1 ../.ci/scripts/timeoutExec.sh -t ${TIMEOUT} -i 1 -d 1 ./bin/${ut}
    if [ $? -ne 0 ]; then
        cp my_cern_project.log ${LOG_DIR}/${ut}_FAILED.log
        retCode=1
    else
        cp my_cern_project.log ${LOG_DIR}/${ut}_PASSED.log
    fi
done
exit ${retCode}
