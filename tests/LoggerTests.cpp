/* Copyright 2017 CERN */

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <fstream>
#include <thread>

#include "Logger/Logger.hpp"

class LoggerShould: public ::testing::Test {
 protected:
  LoggerShould() {
    std::ofstream fileCleaner(LOGGER_FILENAME, std::ios::out | std::ios::trunc);
    fileCleaner.close();
  }
};

TEST_F(LoggerShould, printAsyncMsgs) {  // NOLINT Errors are from Gtest
  utility::Logger logger1("MyLogger1");
  utility::Logger logger2("MyLogger2");

  logger1->info("Some info msg");
  logger2->info("Some info msg");
  logger1->debug("Some debug msg");
  logger2->debug("Some debug msg");

  static const size_t max_number_iterations = 100;
  auto loopPrintFunction = [](const utility::Logger& logger) {
    for (size_t i = 0; i < max_number_iterations; i++) {
      logger->critical("Loop critical msg number: {}", i);
      logger->error("Loop error msg number: {}", i);
      logger->warn("Loop warning msg number: {}", i);
      logger->info("Loop info msg number: {}", i);
      logger->debug("Loop debug msg number: {}", i);
    }
  };

  std::thread t1(loopPrintFunction, logger1);
  std::thread t2(loopPrintFunction, logger2);
  std::thread t3(loopPrintFunction, logger1);
  std::thread t4(loopPrintFunction, logger2);

  t1.join();
  t2.join();
  t3.join();
  t4.join();
}
