#!/bin/bash

function format {
  SRC_DIR="."
  if [ $# -eq 1 ]; then
    SRC_DIR=$1
  fi

  local whitelist="include src"

  for dir in ${whitelist}; do
    path=${SRC_DIR}/${dir}
    find ${path} -type f -iname *.[ch] -o -iname *.[ch]pp -o -iname *.[ch]xx \
      -iname *.cu | xargs -n1 clang-format -i -style=file
  done
}

format $@
